"""!
    @file fp-plot.py
    @page fp-plot.py
    @brief Исходный код программы, строящей графики
    @section dyscription DYSCRIPTION
    @details Программа строит графики на основе словаря данных, получаемых на входном потоке:{ось_абсцисс}:{ось_ординат}.
    @section synopsis SYNOPSIS
    python3 fp-plot.py
    @section author AUTHOR
    Daniel Zagaynov
    @var dict args
    @brief Содержит словарь значений {ось_абсцисс:ось_ординат}.
    @var float width
    @brief Толщина столбцов.
    @var ~.figure.Figure fig
    @var .axes.Axes ax
    @var str label
    @brief Подпись под графиком.
"""

import matplotlib.pyplot as plt
import sys

args = {}
for i in sys.stdin.read().split():
    args[int(i.split(':')[0])] = int(i.split(':')[1])

width = 0.35

for i in range(max(args.keys())):
    if not (i in args.keys()):
        args[i] = 0

fig, ax = plt.subplots()

ax.bar(tuple(args.keys()), tuple(args.values()), width, label='Particles')

ax.set_ylabel('Count')
ax.set_xlabel('Size')
ax.legend()

plt.show()
