import subprocess
import json
import sys
import time
import tabulate

class Test():
    def __init__(self, name):
        self.name = name
        self.passed = False
        self.time = 0

def list_to_dict(lst):
    dic = {}
    for obj in lst:
        dic[int(obj.split(':')[0])] = int(obj.split(':')[1])
    return dic

def check(src, dst, m_error):
    for obj in src.keys():
        cash = 0
        for i in range(-m_error, m_error + 1):
            if obj + i in dst.keys():
                cash += dst[obj + i]
        if cash != src[obj]:
            return False
    return True


with open(sys.argv[2], "r") as f:
    tests = json.load(f)

test_list = []

for test_name, test_data in zip(tests.keys(), tests.values()):
    test_list.append(Test(test_name))
    before = time.time()
    fp = subprocess.run((sys.argv[1], test_data['name']), capture_output = True)
    test_list[-1].passed = check(list_to_dict(test_data['data'].split('\n')), list_to_dict(fp.stdout.decode().split()), 2)
    test_list[-1].time = str(int(time.time() - before)) + ' sec'
    if not ('--full-tests' in sys.argv) and test_name == 'test_2':
        break
    if not ('--extra-full-tests' in sys.argv) and test_name == 'test_9':
        break

data = [['Test Name', 'Passed', 'Time'], ['-' * len('Test Name'), '-' * len('Passed'), '-' * len('Time')]]
for obj in test_list:
    data.append([obj.name, obj.passed, obj.time])
print(tabulate.tabulate(data))

